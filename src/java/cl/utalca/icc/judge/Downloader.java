package cl.utalca.icc.judge;

import cl.utalca.icc.judge.log.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * La clase Downloader provee los métodos necesarios para descargar archivos 
 * desde una URL
 * 
 * @author Pablo Rojas (pabrojas@utalca.cl)
 */
public class Downloader
{
    /**
     * Retorna una referencia al objeto donde se desargó el archivo especificado.
     * 
     * El archivo descargado se almacena en la carpeta espepecificada por 
     * "destinationFolder" y desde la URL se obtiene el nombde del archivo, 
     * para esto se invoca al método Downloader.getFilename(url).
     * 
     * @param source ubicación del archivo a descargar
     * @param destinationFolder carpeta donde se descargará el archivo
     * @return el archivo descargado
     * @throws IOException si ocurre un error de I/O al intentar descargar el
     *          archivo o acceder a la carpeta especificada
     */
    public static File fetch( URL source, File destinationFolder ) throws IOException
    {
        try
        {
            String filename = Downloader.getFilename(source);
            File destinationFile = new File( destinationFolder + File.separator + filename );
            ReadableByteChannel rbc = Channels.newChannel(source.openStream());
            FileOutputStream fos = new FileOutputStream(destinationFile);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            return destinationFile;
        }
        catch( IOException e )
        {
            Log.error("Se produjo un error al descargar el archivo");
            throw e;
        }
    }
    
    /**
     * Retorna el texto que viene a continuación de la última ocurrencia del 
     * caracter "/"
     * 
     * Si la dirección especificada no especifica ningún archivo retorna null
     * 
     * @param url dirección desde la cual se intenta extraer el nombre del archivo
     * @return el noombre del archiv especificado, si existem null en caso contrario
     */
    private static String getFilename( URL url )
    {
        String filename = url.getFile();
        try
        {
            filename = filename.substring( filename.lastIndexOf("/")+1 );
        }
        catch(Exception e)
        {
            filename = null;
        }
        if( filename != null && filename.trim().length() == 0 )
        {
            filename = null;
        }
        return filename;
    }
    
}
