package cl.utalca.icc.judge;

import java.util.List;

/**
 * La clase ExecutionResult almacena toda la información del resultado de una
 * ejecución de un caso de prueba
 * 
 * @author Pablo Rojas (pabrojas@utalca.cl)
 */
public class ExecutionResult
{

    private final TestCase testCase;
    private final List<String> output;
    private final String error;
    private final int exitValue;
    private final boolean onTime;

    /**
     * Constructor de ExecutionResult
     * 
     * @param testCase el caso de prueba con el cual se ejecutó el código proporcionado al Robot
     * @param output la salida estándar de la ejecución para el caso de prueba proporcionado por el Robot
     * @param error la salida de error de la ejecución para el caso de prueba proporcionado por el Robot
     * @param exitValue el código de finalización ontenido de ejecutar el programa por el Robot
     * @param onTime si la ejecución del programa terminó dentro del tiempo especificado por el Robot
     */
    public ExecutionResult(TestCase testCase, List<String> output, String error, int exitValue, boolean onTime)
    {
        this.testCase = testCase;
        this.output = output;
        this.error = error;
        this.exitValue = exitValue;
        this.onTime = onTime;
    }
    
    
    public ExecutionResult(TestCase testCase, List<String> output, int exitValue, boolean onTime)
    {
        this.testCase = testCase;
        this.output = output;
        this.error = null;
        this.exitValue = exitValue;
        this.onTime = onTime;
    }
    
    /**
     * Retorna el caso de prueba de esta ejecución
     * 
     * @return el caso de prueba de esta ejecución
     */
    public TestCase getTestCase()
    {
        return testCase;
    }

    /**
     * Retorna la salida estandar de la ejecución
     * 
     * @return la salida estandar de la ejecución
     */
    public List<String> getOutput()
    {
        return output;
    }

    /**
     * Retorna la salida de error de la ejecución
     * 
     * @return la salida de error de la ejecución
     */
    public String getError()
    {
        return error;
    }

    /**
     * Retorna el código de finalización de la ejecución
     * 
     * @return el código de finalización de la ejecución
     */
    public int getExitValue()
    {
        return exitValue;
    }

    /**
     * Retorna si la ejecución terminó en el tiempo especificado
     * 
     * @return 
     */
    public boolean isOnTime()
    {
        return onTime;
    }

}
