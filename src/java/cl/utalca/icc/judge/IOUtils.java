package cl.utalca.icc.judge;

import cl.utalca.icc.judge.log.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * La clase IOUtils proporciona todos los métodos necesarios para las
 * operaciones de entrada y salida
 *
 * @author Pablo Rojas (pabrojas@utalca.cl)
 */
public class IOUtils
{

    /**
     * El salto de línea específico para el sistema operativo
     */
    public static String NEW_LINE = System.getProperty("line.separator");
    private static final SecureRandom RANDOM = new SecureRandom();

    /**
     * Escribe el contenido de la lista "inputLines" al flujo "outputStream",
     * cada una de las líneas termina con un salto de línea, a excepción de la
     * última
     *
     * @param outputStream flujo donde se escribirá
     * @param inputLines lista de las líneas que se desean escribir
     * @throws IOException si ocurre un error de escritura
     */
    public static void write(OutputStream outputStream, List<String> inputLines) throws IOException
    {
        int nLines = inputLines.size();
        for (int index = 0; index < nLines - 1; index++)
        {
            String line = inputLines.get(index);
            outputStream.write(line.getBytes());
            outputStream.write(NEW_LINE.getBytes());
            outputStream.flush();
        }
        if (nLines > 0)
        {
            String line = inputLines.get(nLines - 1);
            outputStream.write(line.getBytes());
            outputStream.write(NEW_LINE.getBytes());
            outputStream.flush();
        }
    }

    /**
     * Copia el contenido del archivo source al archivo destination
     *
     * @param source archivo a ser copiado
     * @param destination archivo de destino
     * @throws IOException
     */
    public static void copy(File source, File destination) throws IOException
    {
        List<String> content = IOUtils.read(source);
        IOUtils.write(destination, IOUtils.toString(content));
    }

    /**
     * Retorna la conversión de la lista de líneas a un único String.
     *
     * A cada una de las líneas de la lista se les agrega un salto de línea a
     * excepción de la última
     *
     * @param lines las lineas a ser convertidas a un único String
     * @return la conversión de la lista de líneas a un único String
     */
    public static String toString(List<String> lines)
    {
        String returnValue = "";
        int nLines = lines.size();
        for (int index = 0; index < nLines - 1; index++)
        {
            returnValue += lines.get(index) + IOUtils.NEW_LINE;
        }
        if (nLines > 0)
        {
            returnValue += lines.get(nLines - 1);
        }
        return returnValue;
    }

    /**
     * Escribe el contenido del String al archivo especificado
     *
     * @param file archivo donde se ecribirá el contenido
     * @param content contenido a escribir
     * @throws IOException si el no se puede acceder o escribir al archivo
     */
    public static void write(File file, String content) throws IOException
    {
        try
        {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(content.getBytes());
            fos.close();
        }
        catch (IOException e)
        {
            Log.error("No se puede escribir el contenido al archivo");
            throw e;
        }
    }

    /**
     * Retorna el contenido del archivo como una lista de líneas
     *
     * Cada línea del archivo es almacenda en la lista, incluyendo las líneas en
     * blanco
     *
     * @param file archivo a ser leído
     * @return el contenido del archivo almacenado en una lista
     * @throws IOException si no se puede acceder o leer del archivo
     */
    public static List<String> read(File file) throws IOException
    {
        Scanner scanner = new Scanner(file);
        ArrayList<String> lines = new ArrayList<String>();
        while (scanner.hasNextLine())
        {
            lines.add(scanner.nextLine());
        }
        return lines;
    }

    /**
     * Crea una carpeta temporal en el directorio de creación de directorios
     * temporales
     *
     * @return la carpeta temporal recién creada
     */
    public static File createTempFolder(File parent)
    {
        File file = null;
        do
        {
            String srandom = new BigInteger(130, IOUtils.RANDOM).toString(32);
            file = new File(parent.getAbsolutePath() + File.separator + srandom);
        }
        while (file.exists());
        file.mkdirs();
        return file;
    }

    /**
     * Elimina un archivo o directorio
     *
     * @param file archivo o directorio a ser eliminado
     */
    public static void delete(File file)
    {
        if (!file.isDirectory())
        {
            file.delete();
        }
        if (file.isDirectory())
        {
            File[] files = file.listFiles();
            for (File aux : files)
            {
                IOUtils.delete(aux);
            }
        }
        file.delete();
    }

    /**
     * Retorna una lista con cada una de las líneas leidas desde el flujo
     *
     * @param iStream flujo desde donde se leerán las líneas
     * @return la lista con cada una de las líneas leidas desde el flujo
     * @throws IOException si ocurre algún problema con la lectura desde este
     * flujo
     */
    public static List<String> readStream(InputStream iStream) throws IOException
    {
        try
        {
            InputStreamReader iStreamReader = new InputStreamReader(iStream);
            BufferedReader bReader = new BufferedReader(iStreamReader);
            ArrayList<String> lines = new ArrayList<String>();

            String line;
            while ((line = bReader.readLine()) != null)
            {
                lines.add(line);
            }
            bReader.close();
            iStreamReader.close();
            iStream.close();

            return lines;
        }
        catch (IOException e)
        {
            throw e;
        }
    }

    public static String fixPath(String path)
    {
        if (path.charAt(path.length() - 1) != File.separatorChar)
        {
            path = path + File.separatorChar;
        }
        return path;
    }
    
    public static String fixPathName( String pathName )
    {
        return pathName.replace(" ", "\\ ");
    }

    static String writeBash(File directory, String executionCommandString) throws IOException
    {
        File file = null;
        String name = "";
        do
        {
            name = new BigInteger(130, IOUtils.RANDOM).toString(32) + ".sh";
            file = new File(directory.getAbsolutePath() + File.separator + name);
        }
        while (file.exists());
        
        PrintWriter writer = new PrintWriter(file);
        writer.println("#!/bin/bash");
        writer.print(executionCommandString);
        writer.close();
        
        return name;
    }

}
