package cl.utalca.icc.judge;

/**
 * La clase IncorrectFileTypeException es usada para especificar que la extensión
 * de un archivo no corresponde con la extensión esperada
 * 
 * @author Pablo Rojas (pabrojas@utalca.cl)
 */
public class IncorrectFileTypeException extends Exception
{
    public IncorrectFileTypeException(String message)
    {
        super(message);
    }
}
