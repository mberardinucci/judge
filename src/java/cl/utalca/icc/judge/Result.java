package cl.utalca.icc.judge;

/**
 * El enum Result contiene los posibles mensajes que puede responder el Robots
 * 
 * @author Pablo Rojas (pabrojasæutalca.cl)
 */
public enum Result 
{
    COMPILATION_ERROR("Compilation Error"),
    RUNTIME_EXECUTION_ERROR("Runtime Excecution Error"),
    TIME_LIMIT_EXCEEDED("Time Limit Exceeded"),
    OUTPUT_FORMAT_ERROR("Output Format Error"),
    WRONG_ANSWER("Wrong Answer"),
    CORRECT("Correct"),
    UNKWNOWN("There was a problem evaluating this code");
    
    
    private final String key;    

    /**
     * Constructor de enum Result
     * 
     * @param key mensaje del resultado
     */
    private Result( String key )
    {
        this.key = key;
    }

    /**
     * Convierte este objeto enumerado a String, retornando el mensaje del resultado
     * 
     * @return 
     */
    @Override
    public String toString()
    {
        return this.key;
    }
}
