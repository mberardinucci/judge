package cl.utalca.icc.judge;

import cl.utalca.icc.judge.language.Language;
import cl.utalca.icc.judge.log.Log;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 * La clase Robot es la encargada de ejecutar los códigos fuentes para los lenguajes
 * de programación cargados y retornar el resultado de cada uno de los casos de prueba
 * especificados.
 * 
 * Esta clase crea una carpeta temporal y ahí descarga el código fuente y el XML de casos
 * de prueba. Luego compila y ejecuta el programa en esta carpeta, si en la ejecución se
 * crearan archivo se crean en esta carpeta. Al finalizar la ejecución la carpeta se 
 * elimina, borrando recursivamente todo su contenido.
 * 
 * @author Pablo Rojas (pabrojas@utalca.cl)
 */
public class Robot
{
    private static final String VIRTUAL_DIRECTORY = "/home/usuario";
    private static final String DOCKER_COMMAND = "sudo docker run";
    private static final String CONTAINER_NAME = "ubuntu_robot";
    
    
    
    private final Language language;
    
    private final File directory;
    private final File sourceFile;
    private final List<TestCase> testCases;
    private final long maximumRunningTimeMillis;
    
    
    public Robot( File directory, File source, File test, Language language, long maximumRunningTimeMillis ) 
            throws IOException, IncorrectFileTypeException, TestCaseFormatException
    {
        this.directory = directory;
        this.sourceFile = source;
        this.testCases = TestCasesLoader.load(test);
        this.language = language;
        this.maximumRunningTimeMillis = maximumRunningTimeMillis;
        
        if( !language.isValidFile(sourceFile) )
        {
            String message = String.format("El archivo (%s) no tiene la extensión requerida para el lenguaje (%s).", 
                    source, this.language.getDescription());
            Log.error(message);
            throw new IncorrectFileTypeException(message);
        }
        
        initializeWildcards();
    }

    /**
     * Retorna el XML resultante de la ejecución del código fuente para los casos de prueba especificados
     * 
     * @return el XML resultante de la ejecución
     */
    public String test()
    {
        XMLBuilder builder = new XMLBuilder();
        try
        {
            if( this.compile() )
            {
                builder.setSuccessfulCompilation(true);
                List<ExecutionResult> executionResults = this.execute();
                
                for( ExecutionResult executionResult : executionResults )
                {
                    Result result = this.evaluate(executionResult);
                    builder.addCase(executionResult.getTestCase().getId(), result.toString(), executionResult.getOutput());
                }
            }
        }
        catch( Exception e )
        {
            builder.setSuccessfulCompilation(false);
        }
        
        return builder.toString();
    }
    
    /**
     * Retorna si el código fuente compila exitosamente para el lenguaje señalado
     * 
     * @return true si el codigo fuente compila exitosamente o el lenguaje no necesita compilación, false de lo contrario
     * @throws IOException si ocurre un error de acceso a los archivos 
     * @throws InterruptedException si se interrumpe por algún motivo del S.O. la ejecución del comando de compilación
     */
    private boolean compile() throws IOException, InterruptedException
    {
        if( !this.language.needsToBeCompiled())
        {
            return true;
        }
        
        String compilationCommandString = this.language.getCompilationCommand();
        compilationCommandString = this.dockerize(Wildcards.fix(compilationCommandString));

        try
        {
            ProcessBuilder builder = new ProcessBuilder(Robot.toStringList(compilationCommandString));
            Process process = builder.start();
        
            InputStream errorStream = process.getErrorStream();
            InputStream inputStream = process.getInputStream();
            
            String error = IOUtils.toString(IOUtils.readStream(errorStream));            
            String input = IOUtils.toString(IOUtils.readStream(inputStream));
            
            if( process.waitFor()== 0 )
            {               
                return true;
            }
            else
            {                
                Log.warning(error);
                Log.warning(input);
                return false;
            }
        }
        catch( IOException e )
        {
            Log.error("No se puede realizar la compilación : " + compilationCommandString);
            throw e;
        }
        catch( InterruptedException e )
        {
            Log.error("No se puede realizar la compilación : " + compilationCommandString);
            throw e;
        }
    }
    
    /**
     * Retorna una lista de objetos ExecutionResult los cuales tienen la información de los resultados
     * de ejecución del código fuente para cada uno de los casos de prueba.
     * 
     * Este método pimero prepara el comando de compilación y luego ejecuta el código para todos los
     * casos de prueba especificados
     * 
     * @returnla lista de los resultados de ejecución
     * @throws IOException si ocurre un error de acceso a los archivos 
     */
    private List<ExecutionResult> execute() throws IOException, InterruptedException
    {
        String executionCommandString = this.language.getExecutionCommand();
        executionCommandString = Wildcards.fix(executionCommandString);
        
        System.out.println(executionCommandString);
        try
        {
            ArrayList<ExecutionResult> results = new ArrayList<ExecutionResult>();
            for( TestCase testCase : this.testCases )
            {
                ExecutionResult executionResult = this.execute(executionCommandString, testCase);
                System.out.println("executionResult "+executionResult);
                results.add(executionResult);
            }
            return results;
        }
        catch(IOException|InterruptedException e)
        {
            Log.error("No se puede realizar la ejecución: " + executionCommandString);
            throw e;
        }
        
    }
    
    /**
     * Ejecuta el código fuente para un caso de prueba
     * 
     * @param executionCommandString comando de ejecución
     * @param testCase caso de prueba
     * @return retorna el resultado de la ejecución
     * @throws IOException si ocurre un error de acceso a los archivos 
     */
    private ExecutionResult execute( String executionCommandString, TestCase testCase ) throws IOException, InterruptedException
    {
        try
        { 
            //Asigno el nombre del archivo de entrada
            String inputFileName = testCase.getId() + ".in"; 
            if( this.language.hasInputFile() )
            {
                inputFileName = this.language.getInputFileName();
            }
            
            //Guardo los datos al archivo de entrada
            File processInputFile = new File( this.directory + File.separator + inputFileName );
            String inputLines = IOUtils.toString(testCase.getInput());
            IOUtils.write(processInputFile, inputLines);
            
            //Agrego la redireccion de la entrada solo si es necesario
            if( !this.language.hasInputFile() )
            {
                String filename = Robot.VIRTUAL_DIRECTORY + File.separator + inputFileName;
                filename = IOUtils.fixPathName(filename);
                executionCommandString = executionCommandString + " < " + filename; 
            }
            
            //Asigno el nombre del archivo de salida
            String outputFileName = this.language.getOutputFileName();
            
            //Agrego la redireccion de la salida solo si es necesario
            if( !this.language.hasOutputFile() )
            {
                outputFileName = testCase.getId() + ".out";
                String filename = Robot.VIRTUAL_DIRECTORY + File.separator + outputFileName;
                filename = IOUtils.fixPathName(filename);
                executionCommandString = executionCommandString + " > " + filename;
            }
            
            //Genero el bash script para ejecutar el programa y le doy permisos de ejecucion
            String bashFilename = IOUtils.writeBash(this.directory, executionCommandString);
            this.grantExecution(bashFilename);
            
            //Genero el comando para Docker
            String command = Robot.VIRTUAL_DIRECTORY + File.separator + bashFilename;
            command = this.dockerize(command);
            System.out.println(command);
            ProcessBuilder builder = new ProcessBuilder(Robot.toStringList(command));
            
            //Comienzo el proceso
            long startTime = System.currentTimeMillis();
            Process process = builder.start();
            
            //Espero a que termine normal o lo termino por tiempo
            boolean normalFinished = this.checkLimitTime( process, startTime );
            int exitValue = process.exitValue();
            
            //Leo la salida del proceso desde el archivo donde se redirecciono la salida 
            File processOutputFile = new File( this.directory + File.separator + outputFileName );
            List<String> output = IOUtils.read(processOutputFile );
            
            
            System.out.println("------------------------------------");
            System.out.println("ID: " + testCase.getId());
            System.out.println("salida esperada:");
            System.out.println( IOUtils.toString(testCase.getOutput()));
            System.out.println("salida proceso:");
            System.out.println( IOUtils.toString(output));
            System.out.println("------------------------------------");
            
            
            return new ExecutionResult(testCase, output, exitValue, normalFinished);
        }
        catch( IOException | InterruptedException e )
        {
            throw e;
        }
    }
    
    /**
     * Retorna si la ejecución del código fuente terminó dentro del tiempo especificado.
     * 
     * Este método ejecuta el código fuente y verifica que la ejecución se realice dentro 
     * del tiempo especificado para la ejecución, de lo contrario termina el proceso de ejecución
     * 
     * @param process proceso resultante de la ejecución del programa
     * @param startTime milisegunso desde el epoch time de cuando empezó la ejecución
     * @return true si la ejecución del código fuente se realizó dentro del tiempo especificado, false de lo contrario
     */
    private boolean checkLimitTime( Process process, long startTime ) 
    {
        boolean normalFinished = false;
        boolean finished = false;
        while( !finished )
        {
            try
            {
                process.exitValue();
                finished = true;
                normalFinished = true;
            }
            catch( IllegalThreadStateException ex )
            {
            }
            long actualTime = System.currentTimeMillis();
            if( actualTime - startTime > this.maximumRunningTimeMillis )
            {
                finished = true;
                process.destroy();
            }
        }
        return normalFinished;
    }
    
    /**
     * Retorna el resultado de la evaluación de la respuesta de ejecutar el código fuente
     * 
     * @param execution objeto con la información de ejecución
     * @return el resultado de la evaluación de la respuesta de ejecutar el código fuente
     */
    private Result evaluate( ExecutionResult execution )
    {
        if( !execution.isOnTime() )
        {
            return Result.TIME_LIMIT_EXCEEDED;
        }
        if( execution.getExitValue() != 0 )
        {
            return Result.RUNTIME_EXECUTION_ERROR;
        }
        
        String testOutputFilename = String.format("%s.test.out",Wildcards.CLASSNAME.value + execution.getTestCase().getId());
        String processOutputFilename = String.format("%s.process.out",Wildcards.CLASSNAME.value + execution.getTestCase().getId());
        
        File testOutputFile = new File( this.directory + File.separator + testOutputFilename );
        File processOutputFile = new File( this.directory + File.separator + processOutputFilename );
        
        try
        {
            IOUtils.write(testOutputFile, IOUtils.toString(execution.getOutput()));
            IOUtils.write(processOutputFile, IOUtils.toString(execution.getTestCase().getOutput()));
            
            Runtime runtime = Runtime.getRuntime();
            String diff = String.format("diff %s %s", testOutputFile.getAbsolutePath(), processOutputFile.getAbsolutePath());
            Process process = runtime.exec(diff);
            process.waitFor();
            InputStream inputStream = process.getInputStream();
            String input1 = IOUtils.toString(IOUtils.readStream(inputStream));
            inputStream.close();
            
            if( input1.length() == 0 )
            {
                return Result.CORRECT;
            }
            else
            {
                diff = String.format("diff -ibB %s %s", testOutputFile.getAbsolutePath(), processOutputFile.getAbsolutePath());
                process = runtime.exec(diff);
                process.waitFor();
                inputStream = process.getInputStream();
                String input2 = IOUtils.toString(IOUtils.readStream(inputStream));
                if( input2.length() == 0 )
                {
                    return Result.OUTPUT_FORMAT_ERROR;
                }
                else
                {
                    return Result.WRONG_ANSWER;
                }
            }
            
        }
        catch( Exception e )
        {
            return Result.UNKWNOWN;
        }
        
    }

    /**
     * Inicializa las Wildcars de la definición de los lenguajes con los valores obtenidos
     * de esta instancia de Robot
     * 
     * @throws IOException si ocurre algún error de acceso a los archivos 
     */
    private void initializeWildcards() throws IOException
    {
        try
        {
            Wildcards.FILENAME.setValue(IOUtils.fixPath(Robot.VIRTUAL_DIRECTORY) + this.sourceFile.getName());
            String className = this.sourceFile.getName();
            className = className.substring(0, className.lastIndexOf("."));
            Wildcards.CLASSNAME.setValue(className);
            Wildcards.PATH.setValue(Robot.VIRTUAL_DIRECTORY);
        } 
        /*catch( IOException e )
        {
            Log.error("Robot", "Error inicializando las Wildcards, no se puede acceder a los archivos/directorios creados");
            throw e;
        }*/
        catch( IndexOutOfBoundsException e )
        {
            Log.error("Error inicializando las Wildcards, no se puede determinar el valor para Wildcards.CLASSNAME");
        }
    }

    private void grantExecution(String bashFilename) throws IOException, InterruptedException
    {
        String command = "sudo chmod 777 " + this.directory.getAbsolutePath() + File.separator + bashFilename;
        ProcessBuilder builder = new ProcessBuilder(Robot.toStringList(command));
        Process process = builder.start();
        process.waitFor();
    }
        
    /**
     * Este tipo enumerado tiene todos los posibles comodines de la definición de los lenguajes de programación 
     * y los valores por los cuales se debe reemplazar
     */
    private enum Wildcards
    {
        FILENAME("%file%"),
        CLASSNAME("%class%"),
        PATH("%path%");
        
        
        private final String key;
        private String value;
        
        private Wildcards( String key )
        {
            this.key = key;
        }
        
        public void setValue( String value )
        {
            this.value = value;
        }
        
        @Override
        public String toString()
        {
            return this.key;
        }
        
        public static String fix( String command )
        {
            for( Wildcards wildcard : Wildcards.values() )
            {
                command = command.replace(wildcard.key, wildcard.value);
            }
            return command;
        }
    }
        
    /**
     * Retorna el resultado de obtener todos los tokens del String command y 
     * pasarlo a un ArrayList
     * 
     * @param command texto a convertir
     * @return lista con los tokens de command
     */
    private static ArrayList<String> toStringList( String command )
    {
        StringTokenizer tokenizer = new StringTokenizer(command);
        ArrayList<String> list = new ArrayList<String>();
        while(tokenizer.hasMoreTokens())
        {
            String token = tokenizer.nextToken();
            list.add(token);
        }
        return list;
    }
    
    private String dockerize( String command )
    {
        String dockerizedComand = Robot.DOCKER_COMMAND + " -v " + 
                this.directory + ":" + Robot.VIRTUAL_DIRECTORY + " " +
                Robot.CONTAINER_NAME + " " + command;
        return dockerizedComand;
    }
    
}
