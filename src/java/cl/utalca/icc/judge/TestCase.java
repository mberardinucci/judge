package cl.utalca.icc.judge;

import java.util.ArrayList;
import java.util.List;

/**
 * La clase TestCase almacena la configuración de un caso de prueba, en concordancia
 * con el archivo XML de casos de prueba.
 * 
 * @author Pablo Rojas (pabrojas@utalca.cl)
 */
public class TestCase
{
    private String id;
    private ArrayList<String> inputLines;
    private ArrayList<String> outputLines;
    
    /**
     * Constructor de la clase TestCase
     */
    public TestCase()
    {
        this.inputLines = new ArrayList<String>();
        this.outputLines = new ArrayList<String>();
    }
    
    /**
     * Constructor de la clase TestCase
     * 
     * @param id identificador del caso de prueba
     */
    public TestCase( String id )
    {
        this();
        this.id = id;
    }
    
    /**
     * Asigna un identificador al caso de prueba
     * 
     * @param id identificador del caso de prueba
     */
    public void setId( String id )
    {
        this.id = id;
    }

    /**
     * Retorna el identificador de este caso de prueba 
     * 
     * @return el nombre del caso de prueba
     */
    public String getId()
    {
        return id;
    }
    
    /**
     * Retorna true si logra agreagr un nueva línea a la entrada definida para
     * este caso de prueba.
     * 
     * Cada una de las líneas agregadas se anexan al final, conservando el orden 
     * en el que van siendo agregadas
     * 
     * @param line una linea a la entrada del caso de prueba
     * @return true si logra agregar la línea, false de lo contrario
     */
    public boolean addInputLine( String line )
    {
        return this.inputLines.add(line);
    }
    
    /**
     * Retorna true si logra agreagr un nueva línea a la salida definida para
     * este caso de prueba.
     * 
     * Cada una de las líneas agregadas se anexan al final, conservando el orden 
     * en el que van siendo agregadas
     * 
     * @param line una linea a la salida del caso de prueba
     * @return true si logra agregar la línea, false de lo contrario
     */
    public boolean addOutputLine( String line )
    {
        return this.outputLines.add(line);
    }
    
    /**
     * Retorna las lineas de entrada del caso de prueba
     * 
     * @return las lineas de entrada del caso de prueba
     */
    public List<String> getInput()
    {
        return (List<String>)this.inputLines.clone();
    }
    
    /**
     * Retorna las líneas de salida del caso de prueba
     * 
     * @return las líneas de salida del caso de prueba
     */
    public List<String> getOutput()
    {
        return (List<String>)this.outputLines.clone();
    }
}
