package cl.utalca.icc.judge;

/**
 * La clase TestCaseFormatException es usada para especificar que el caso de prueba
 * leido no respeta el formato establecido.
 * 
 * @author Pablo Rojas (pabrojas@utalca.cl)
 */
public class TestCaseFormatException extends Exception
{
    public TestCaseFormatException(String message)
    {
        super(message);
    }
}
