package cl.utalca.icc.judge;

import cl.utalca.icc.judge.log.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * La clase TestCasesLoader tiene todos los métodos necesarios para leer el XML
 * de los casos de prueba y convertirlos en objetos TestCase
 * 
 * @author Pablo Rojas (pabrojas@utalca.cl)
 */
public class TestCasesLoader
{
    /**
     * Valores de las etiquetas y atributos de los archivos de casos de prueba
     */
    private enum TestCaseTags
    {
        TEST_CASES("TestCases"), 
        CASE("Case"), 
        INPUT_LINES("InputLines"),
        OUTPUT_LINES("OutputLines"),
        LINE("Line"),
        ID("id");
        
        private String key;
        private TestCaseTags( String key )
        {
            this.key = key;
        }
        
        @Override
        public String toString()
        {
            return this.key;
        }
    }

    /**
     * Retorna una lista de objetos TestCase los cuales contienen la información de 
     * cada uno de los casos de prueba definidos en el archivo pasado como parámetro.
     * 
     * Este archivo debe respetar el siguiente formato XML:
     * 
     * <?xml version="1.0"?>
     * <TestCases>
     *    <Case id="id_1">
     *       <InputLines>
     *          <Line>Input Line</Line>
     *       </InputLines>
     *       <OutputLines>
     *          <Line>Output Line</Line>
     *       </OutputLines>
     *    </Case>
     * </TestCases>   
     * 
     * Consideraciones:
     *      - el archivo puede contener múltiples elementos Case, pero el identificador
     *          de estos casos de prueba debe ser único en el documento, coincidiendo 
     *          mayúsculas y minúsculas. De existir algún caso de prueba o un identificador 
     *          previamente usado se retornará una excepción del tipo TestCaseFormatException
     *      - los elementos InputLines y OutputLines pueden tener múltiples etiquetas Line
     * 
     * @param xml archivo de casos de prueba a ser cargado
     * @return la lista de los casos de prueba especificados en el archivo
     * @throws TestCaseFormatException en el caso de que el archivo no respete el formato XML señalado
     */
    static public List<TestCase> load(File xml) throws TestCaseFormatException
    {
        ArrayList<TestCase> cases = new ArrayList<TestCase>();
        try
        {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xml);

            doc.getDocumentElement().normalize();
            
            Node testCases = doc.getChildNodes().item(0);
            if( testCases.getNodeName().equals(TestCaseTags.TEST_CASES.toString()) )
            {
                NodeList nodeList = testCases.getChildNodes();
                for (int i = 0; i < nodeList.getLength(); i++)
                {
                    Node node = nodeList.item(i);
                    if( node.getNodeType() == Node.ELEMENT_NODE )
                    {
                        if( node.getNodeName().equals(TestCaseTags.CASE.toString()) )
                        {
                            Element testCaseElement = (Element)node;
                            TestCase testCase = TestCasesLoader.createTestCase(testCaseElement);
                            if( TestCasesLoader.containsId( cases, testCase.getId() ) == true )
                            {
                                String message = String.format("El documento tiene id repetido: '%s'", testCase.getId() );
                                Log.error(message);
                                throw new TestCaseFormatException(message);
                            }
                            cases.add(testCase);
                        }
                        else
                        {
                            String message = String.format("El documento tiene etiqueta que no corresponde a '%s': '%s'", TestCaseTags.CASE.toString(), node.getNodeName() );
                            Log.error(message);
                            throw new TestCaseFormatException(message);
                        }
                    }
                    
                }
            }
            else
            {
                String message = String.format("El documento no tiene etiqueta '%s'", TestCaseTags.TEST_CASES.toString());
                Log.error(message);
                throw new TestCaseFormatException(message);
            }
        } 
        catch (ParserConfigurationException | SAXException | IOException ex)
        {
            String message = "No se puede cargar el archivo especificado: " + xml.getAbsolutePath();
            Log.error(message);
        }

        if( cases.size() > 0 )
        {
            return cases;
        }
        return null;
    }
    
    /**
     * Retorna un objeto TestCase leido desde el Element pasado como parámetro
     * 
     * @param testCaseElement objeto Element desde donde se leerá el caso de prueba
     * @return el caso de prueba leido
     * @throws TestCaseFormatException en caso de no cumplir con el formato 
     */
    private static TestCase createTestCase( Element testCaseElement )throws TestCaseFormatException
    {
        int numberOfInputLinesTag = 0;
        int numberOfOutputLinesTag = 0;
        String id = testCaseElement.getAttribute(TestCaseTags.ID.toString());
        TestCase testCase = new TestCase( id );
        NodeList testCaseChilds=  testCaseElement.getChildNodes();
        for (int j = 0; j < testCaseChilds.getLength(); j++)
        {
            Node otherNode = testCaseChilds.item(j);
            if( otherNode.getNodeType() == Node.ELEMENT_NODE )
            {
                if( otherNode.getNodeName().equals(TestCaseTags.INPUT_LINES.toString()) )
                {
                    NodeList inputLines = ((Element)otherNode).getElementsByTagName(TestCaseTags.LINE.toString());
                    TestCasesLoader.addInputLines(testCase, inputLines);
                    numberOfInputLinesTag++;
                }
                else if( otherNode.getNodeName().equals(TestCaseTags.OUTPUT_LINES.toString()) )
                {
                    NodeList inputLines = ((Element)otherNode).getElementsByTagName(TestCaseTags.LINE.toString());
                    TestCasesLoader.addOutputLines(testCase, inputLines);
                    numberOfOutputLinesTag++;
                }
            }
        }
        if( numberOfInputLinesTag != 1 )
        {
            String message = String.format("El documento no tiene etiqueta '%s'",TestCaseTags.INPUT_LINES.toString());
            Log.error(message);
            throw new TestCaseFormatException(message);
        }
        if( numberOfOutputLinesTag != 1 )
        {
            String message = String.format("El documento no tiene etiqueta '%s'",TestCaseTags.OUTPUT_LINES.toString());
            Log.error(message);
            throw new TestCaseFormatException(message);
        }
        return testCase;
    }
    
    /**
     * Lee las líneas del objeto NodeList lines y las agrega como líneas de entrada
     * del caso de prueba
     * 
     * @param testCase caso de prueba al cual se le agregarán las líneas de entrada
     * @param lines objeto NodeList desde donde se leerán las lineas
     * @throws TestCaseFormatException en caso de no cumplir formato
     */
    private static void addInputLines( TestCase testCase, NodeList lines ) throws TestCaseFormatException
    {
        for (int k = 0; k < lines.getLength(); k++)
        {
            Node inputNode = lines.item(k);
            if( inputNode.getNodeType() == Node.ELEMENT_NODE )
            {
                if( inputNode.getNodeName().equals(TestCaseTags.LINE.toString()) )
                {
                    testCase.addInputLine(inputNode.getTextContent());
                }
                else
                {
                    String message = String.format("El docuemnto, dentro de la eqtiqueta '%s' tiene una etiqueta que no respeta formato: '%s'", 
                                    TestCaseTags.INPUT_LINES.toString(), inputNode.getNodeName() );
                    Log.error(message);
                    throw new TestCaseFormatException(message);
                }
            }
        }
    }
    
    /**
     * Lee las líneas del objeto NodeList lines y las agrega como líneas de salida
     * del caso de prueba
     * 
     * @param testCase caso de prueba al cual se le agregarán las líneas de salida
     * @param lines objeto NodeList desde donde se leerán las lineas
     * @throws TestCaseFormatException en caso de no cumplir formato
     */
    private static void addOutputLines( TestCase testCase, NodeList lines ) throws TestCaseFormatException
    {
        for (int k = 0; k < lines.getLength(); k++)
        {
            Node outputNode = lines.item(k);
            if( outputNode.getNodeType() == Node.ELEMENT_NODE )
            {
                if( outputNode.getNodeName().equals(TestCaseTags.LINE.toString()) )
                {
                    testCase.addOutputLine(outputNode.getTextContent());
                }
                else
                {
                    String message = String.format("El docuemnto, dentro de la eqtiqueta '%s' tiene una etiqueta que no respeta formato: '%s'", 
                                    TestCaseTags.OUTPUT_LINES.toString(), 
                                    outputNode.getNodeName() );
                    Log.error(message);
                    throw new TestCaseFormatException( message );
                }
            }
        }
    }
    
    /**
     * Retorna si la lista de casos de prueba contiene un caso de prueba con el mismo
     * identificador que el caso de prueba testCase
     * 
     * @param cases lista de casos de prueba
     * @param id identificador a buscar
     * @return true si la lista contiene un elemento con el identificador especificado,
     *          false de lo contrario
     */
    private static boolean containsId(List<TestCase> cases, String id)
    {
        for( TestCase test : cases )
        {
            if( test.getId().equals(id) )
            {
                return true;
            }
        }
        return false;
    }
}
