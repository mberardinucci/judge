/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.utalca.icc.judge;

import java.util.ArrayList;
import java.util.List;

/**
 * La clase XMLBuilder es usada para construir el XML de respuesta del Robot
 *
 * @author Pablo Rojas (pabrojas@utalca.cl)
 */
public class XMLBuilder {

    static private final String XML_DEFINITION = "<?xml version=\"1.0\"?>";
    static private final String EXECUTION_OPEN = "<Execution successful-compilation=\"%s\">";
    static private final String EXECUTION_CLOSE = "</Execution>";
    static private final String CASE_OPEN = "\t<Case id=\"%s\" result=\"%s\">";
    static private final String CASE_CLOSE = "\t</Case>";
    static private final String OUTPUT_LINES_OPEN = "\t\t<OutputLines>";
    static private final String OUTPUT_LINES_CLOSE = "\t\t</OutputLines>";
    static private final String LINE = "\t\t\t<Line>%s</Line>";

    private final ArrayList<String> cases;
    private String successfulCompilation;

    /**
     * Constructor de XMLBuilder
     */
    public XMLBuilder() {
        this.cases = new ArrayList<String>();
    }

    /**
     * Asigna el valor para el atributo successful-compilation de la etiqueta
     * Execution
     *
     * @param succesfulCompilation
     */
    public void setSuccessfulCompilation(boolean succesfulCompilation) {
        this.successfulCompilation = ((Boolean) succesfulCompilation).toString();
    }

    /**
     * Agregar un resultado para un caso de prueba
     *
     * @param id identificador del caso de prueba, en concordancia con el XML de
     * casos de prueba
     * @param result valor del resultado del caso de prueba
     * @param outputLines resultado de la ejecución del código ejecutado
     */
    public void addCase(String id, String result, List<String> outputLines) {
        String resultCase = String.format(XMLBuilder.CASE_OPEN, id, result) + IOUtils.NEW_LINE;

        resultCase += XMLBuilder.OUTPUT_LINES_OPEN + IOUtils.NEW_LINE;
        for (String output : outputLines) {
            resultCase += String.format(XMLBuilder.LINE, output) + IOUtils.NEW_LINE;
        }
        resultCase += XMLBuilder.OUTPUT_LINES_CLOSE + IOUtils.NEW_LINE;

        resultCase += XMLBuilder.CASE_CLOSE;
        this.cases.add(resultCase);
    }

    /**
     * Retorna el XML generado
     *
     * @return el XML generado
     */
    @Override
    public String toString() {
        String result = XMLBuilder.XML_DEFINITION + IOUtils.NEW_LINE;
        result += String.format(XMLBuilder.EXECUTION_OPEN, this.successfulCompilation) + IOUtils.NEW_LINE;

        for (String executionCase : this.cases) {
            result += executionCase + IOUtils.NEW_LINE;
        }

        result += XMLBuilder.EXECUTION_CLOSE + IOUtils.NEW_LINE;
        return result;

    }
}
