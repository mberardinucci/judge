package cl.utalca.icc.judge.language;

import java.io.File;

/**
 * La clase Language almacena la configuración de un lenguaje válido para el Robot
 * 
 * @author Pablo Rojas (pabrojas@utalca.cl)
 */
public class Language implements Cloneable
{
    private String description;
    private String compilationCommand;
    private String executionCommand;
    private String[] validExtensions;
    private String outputFileName;
    private String inputFileName;

    /**
     * Constructor de Lenguage
     */
    public Language()
    {
        this.compilationCommand = null;
        this.outputFileName = null;
        this.inputFileName = null;
    }

    /**
     * Retorna la descripción del lenguaje
     * 
     * @return la descripción del lenguaje
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Asigna una nueva descripción al lenguaje 
     * 
     * @param description la nueva descripción del lenguaje
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Retorna el String de compilación
     * 
     * @return el String de compilación
     */
    public String getCompilationCommand()
    {
        return compilationCommand;
    }

    /**
     * Asigna un nuevo comando de compilación
     * 
     * @param compilationCommand el nuevo comando de compilación
     */
    public void setCompilationCommand(String compilationCommand)
    {
        this.compilationCommand = compilationCommand;
    }

    /**
     * Retorna el comando de ejecución
     * 
     * @return el comando de ejecución
     */
    public String getExecutionCommand()
    {
        return executionCommand;
    }

    /**
     * Asigna un nuevo comando de ejecución
     * 
     * @param executionCommand el nuevo comando de ejecución
     */
    public void setExecutionCommand(String executionCommand)
    {
        this.executionCommand = executionCommand;
    }

    /**
     * Retorna las extensiones de archivo válidas 
     * 
     * @return las extensiones de archivo válidas 
     */
    public String[] getValidExtensions()
    {
        return validExtensions;
    }

    /**
     * Asigna las nuevas extensiones de archivo válidas 
     * 
     * @param validExtensions las nuevas extensiones de archivo válidas 
     */
    public void setValidExtensions(String[] validExtensions)
    {
        this.validExtensions = validExtensions;
    }

    /**
     * Retorna el nombre del arvhivo de salida del programa ejecutado
     * 
     * @return el nombre del arvhivo de salida del programa ejecutado
     */
    public String getOutputFileName()
    {
        return outputFileName;
    }

    /**
     * Asigna el nuevo nombre del arvhivo de salida del programa ejecutado
     * 
     * @param outputFileName el nuevo nombre del arvhivo de salida del programa ejecutado
     */
    public void setOutputFileName(String outputFileName)
    {
        this.outputFileName = outputFileName;
    }

    /**
     * Retorna el nombre del archivo de entrada del programa ejecutado
     * 
     * @return el nombre del archivo de entrada del programa ejecutado
     */
    public String getInputFileName()
    {
        return inputFileName;
    }

    /**
     * Asigna el nuevo nombre del archivo de entrada del programa ejecutado
     * 
     * @param inputFileName el nuevo nombre del archivo de entrada del programa ejecutado
     */
    public void setInputFileName(String inputFileName)
    {
        this.inputFileName = inputFileName;
    }
    
    /**
     * Retorna si el programa necesita ser compilado
     * 
     * @return true si el programa necesita ser compilado, false de lo contrario
     */
    public boolean needsToBeCompiled()
    {
        return this.compilationCommand != null;
    }
    
    /**
     * Retorna si el programa tiene un archivo de salida
     * 
     * @return true si el programa tiene un archivo de salida, false de lo contrario
     */
    public boolean hasOutputFile()
    {
        return this.outputFileName != null;
    }
    
    /**
     * Retorna si el programa tiene un archivo de entrada
     * 
     * @return true si el programa tiene un archivo de entrada, false de lo contrario
     */
    public boolean hasInputFile()
    {
        return this.inputFileName != null;
    }
    
    /**
     * Retorna si el archivo pasado como parámetro tiene una extensión válida
     * 
     * @param file archivo a ser comprobado
     * @return true si el archivo tiene una extensión válida para este lenguaje, false de lo contrario
     */
    public boolean isValidFile(File file)
    {
        if( !file.isFile() )
            return false;
        
        String filename = file.getName();
        for( String extension : this.validExtensions )
        {
            if( filename.endsWith(extension) )
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public Language clone()
    {
        String[] clonedValidExtensions = new String[this.validExtensions.length];
        for (int i = 0; i < this.validExtensions.length; i++)
        {
            clonedValidExtensions[i] = this.validExtensions[i];
        }
        
        Language language = new Language();
        language.setDescription(        this.description        != null ? new String(this.description)          : null);
        language.setCompilationCommand( this.compilationCommand != null ? new String(this.compilationCommand)   : null);
        language.setExecutionCommand(   this.executionCommand   != null ? new String(this.executionCommand)     : null);
        language.setOutputFileName(     this.outputFileName     != null ? new String(this.outputFileName)       : null);
        language.setInputFileName(      this.inputFileName      != null ? new String(this.inputFileName)        : null);
        language.setValidExtensions(clonedValidExtensions);
        
        return language;
    }
    
    
    
}
