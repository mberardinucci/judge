package cl.utalca.icc.judge.language;

/**
 * La clase LanguageDescriptorFormatException es usada para especificar que el archivo
 * properties de un lenguaje no respeta el formato especificado.
 * 
 * @author Pablo Rojas (pabrojas@utalca.cl)
 */
public class LanguageDescriptorFormatException extends Exception
{
    public LanguageDescriptorFormatException(String message)
    {
        super(message);
    }
}
