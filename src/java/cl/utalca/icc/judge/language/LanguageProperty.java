/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.utalca.icc.judge.language;

/**
* El tipo enumerado LanguageProperty contiene los posibles claves con las que se
* representa un lenguaje de programación en un archivo de propiedades
*/
enum LanguageProperty
{
   DESCRIPTION("description", true),
   COMPILATION_COMMAND("compilation-command", false),
   EXECUTION_COMMAND("execution-command", true), 
   SUPPORTED_EXTENSIONS("supported-extensions", true),
   INPUT("input", false),
   OUTPUT("output", false);

   private String key;
   private boolean required;

   /**
    * Constructor del tipo enumerado LanguageProperty
    * 
    * @param key valor de la propiedad
    * @param required si esta etiqueta debe obligatoriamente estar presente en el archivo de propiedades
    */
   private LanguageProperty( String key, boolean required )
   {
       this.key = key;
       this.required = required;
   }

   /**
    * Convierte este tipo enumerado a String retornando el valor de la propiedad
    * 
    * @return el valor de la propiedad
    */
   @Override
   public String toString()
   {
       return this.key;
   }

   /**
    * Retorna si esta propiedad obligatoriamente debe estar presente en el archivo de propiedades
    * 
    * @return true si la propiedad es requerida, false en otro caso
    */
   public boolean isRequired()
   {
       return this.required;
   }
}