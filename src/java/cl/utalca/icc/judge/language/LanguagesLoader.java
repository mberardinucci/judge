package cl.utalca.icc.judge.language;

import cl.utalca.icc.judge.log.Log;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * La clase LanguageLoader contiene todos los métodos necesarios para leer los
 * archivos de propiedades de los lenguajes soportados y los convierte a un 
 * objeto del tipo Language
 * 
 * @author Pablo Rojas (pabrojas@utalca.cl)
 */
public class LanguagesLoader
{
    /**
     * El tipo enumerado LanguageProperty contiene los posibles claves con las que se
     * representa un lenguaje de programación en un archivo de propiedades
     */
    private enum LanguageProperty
    {
        DESCRIPTION("description", true),
        COMPILATION_COMMAND("compilation-command", false),
        EXECUTION_COMMAND("execution-command", true), 
        SUPPORTED_EXTENSIONS("supported-extensions", true),
        INPUT("input", false),
        OUTPUT("output", false);
        
        private String key;
        private boolean required;
        
        /**
         * Constructor del tipo enumerado LanguageProperty
         * 
         * @param key valor de la propiedad
         * @param required si esta etiqueta debe obligatoriamente estar presente en el archivo de propiedades
         */
        private LanguageProperty( String key, boolean required )
        {
            this.key = key;
            this.required = required;
        }
        
        /**
         * Convierte este tipo enumerado a String retornando el valor de la propiedad
         * 
         * @return el valor de la propiedad
         */
        @Override
        public String toString()
        {
            return this.key;
        }
        
        /**
         * Retorna si esta propiedad obligatoriamente debe estar presente en el archivo de propiedades
         * 
         * @return true si la propiedad es requerida, false en otro caso
         */
        public boolean isRequired()
        {
            return this.required;
        }
    }
    
    /**
     * Carga todos las definiciones de lenguajes disponibles en la carpeta pasada como parámetro.
     * 
     * Si el parámetro no corresponde a un directorio retorna un mapa vacío. Si en el directorio
     * existen varios archivos que contengan la misma descripción solamente carga el primero que encuentre.
     * 
     * @return mapa con los lenguajes cargados, la clave del mapa es la descripción del lenguaje dentro del archivo
     */
    static public Map<String, Language> load( File directory )
    {
        HashMap<String, Language> map = new HashMap<String, Language>();
        
        if( !directory.isDirectory() )
        {
            Log.error("El parámetro pasado no es un directorio");
            return map;
        }
        
        File[] files = directory.listFiles(new FileFilter()
        {
            @Override
            public boolean accept(File pathname)
            {
                return !pathname.isDirectory() && pathname.getName().endsWith(".properties");
            }
        });
        
        for( File file : files )
        {
            try
            {
                InputStream input = new FileInputStream(file);
                Properties properties = new Properties();
                properties.load(input);
                
                Language language = LanguagesLoader.createLanguage(properties);
                if( language != null )
                {
                    if( !map.containsKey(language.getDescription()) )
                    {
                        map.put( language.getDescription(), language );
                    }
                    else
                    {
                        String message = String.format("Ya existe un lenguaje con la misma descripción: '%s' (%s)", language.getDescription(), file.getAbsoluteFile());
                        Log.warning(message);
                    }
                }
                else
                {
                    String message = String.format("El archivo no respeta el formato : '%s'", file.getAbsoluteFile());
                    Log.warning(message);
                }
                
            }
            catch( LanguageDescriptorFormatException e )
            {
                String message = String.format("No se puede cargar el archivo, formato incorrecto: '%s'", file.getAbsoluteFile());
                Log.error(message);
            }
            catch(IOException e)
            {
                String message = String.format("No se puede cargar el archivo: '%s'", file.getAbsoluteFile());
                Log.error(message);
            }
        }
        
        return map;
    }
    
    /**
     * Valida si el archivo de propiedades pasado como parámtro contiene las propiedades obligatorias
     * 
     * @param properties archivo de propiedades a ser validado
     * @return true si el archivo de propiedades contiene todas las propiedades obligatorias, false de lo contrario
     */
    private static boolean validate( Properties properties )
    {
        for( LanguageProperty property : LanguageProperty.values() )
        {
            if( property.isRequired() )
            {
                boolean containsProperty = properties.containsKey(property.toString());
                if( !containsProperty )
                {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Este método recibe como parámetro un objeto Properties y, de ser valido, crea un 
     * objeto del tipo Language con sus correspondientes propiedaes.
     * 
     * @param properties objeto a ser convertido a Language
     * @return el obbjeto Language creado
     * @throws LanguageDescriptorFormatException en caso de que el objeto Properties no sea válido
     */
    private static Language createLanguage( Properties properties ) throws LanguageDescriptorFormatException
    {
        if( !LanguagesLoader.validate(properties) )
        {
            throw new LanguageDescriptorFormatException("Formato Incorrecto");
        }
        
        Language language = new Language();
        
        
        boolean containsCompilationKey = properties.containsKey(LanguageProperty.COMPILATION_COMMAND.toString());
        boolean containsOutputKey = properties.containsKey(LanguageProperty.OUTPUT.toString());
        boolean containsInputKey = properties.containsKey(LanguageProperty.INPUT.toString());
        
        String description = properties.getProperty(LanguageProperty.DESCRIPTION.toString());
        language.setDescription(description);
        
        String executionCommand = properties.getProperty(LanguageProperty.EXECUTION_COMMAND.toString());
        language.setExecutionCommand(executionCommand);
        
        String supportedExtensionsString = properties.getProperty(LanguageProperty.SUPPORTED_EXTENSIONS.toString());
        String[] supportedExtensions = LanguagesLoader.createExtensionsArray(supportedExtensionsString);
        language.setValidExtensions(supportedExtensions);
        
        if( containsCompilationKey )
        {
            String compilationCommand = properties.getProperty(LanguageProperty.COMPILATION_COMMAND.toString());
            language.setCompilationCommand(compilationCommand);
        }
        
        if( containsOutputKey )
        {
            String output = properties.getProperty(LanguageProperty.OUTPUT.toString());
            language.setOutputFileName(output);
        }
        
        if( containsInputKey )
        {
            String input = properties.getProperty(LanguageProperty.INPUT.toString());
            language.setInputFileName(input);
        }
        
        return language;
    }
    
    /**
     * Convierte un String de separado por comas a un arreglo de String, donde cada 
     * token entre comas es agregado al arreglo, sin incluir valores nulos y quitando 
     * los espacios blancos al principio y final de cada token
     * 
     * @param source String con tokens separados por coma
     * @return 
     */
    private static String[] createExtensionsArray( String source )
    {
        ArrayList<String> extensions = new ArrayList<String>();
        String[] tokens = source.split(",");
        for( String token : tokens )
        {
            if( token != null )
            {
                extensions.add(token.trim());
            }
        }
        return extensions.toArray( new String[extensions.size()] );
    }
    
    
}
