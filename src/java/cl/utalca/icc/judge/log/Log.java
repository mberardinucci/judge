
package cl.utalca.icc.judge.log;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * La clase Log contiene todos los métodos necesarios para registrar los mensajes
 * de bitácora de lo que ocurre con la librería ExecutionRobot.
 * 
 * Implementa el patrón de diseño Singleton y sobre esta instancía única escribe todo 
 * lo necesario. Todos los registros se escriben en un archivo diario y además dentro 
 * se especifica la fecha y hora del registro.
 * 
 * @author Pablo Rojas (pabrojas@utalca.cl)
 */
public class Log
{
    static private String logsDirectoryPathName = "logs/";
    static private PrintWriter writerInstance;
    
    static
    {
        File directory = new File(Log.logsDirectoryPathName);
        directory.mkdirs();
    }
    
    /**
     * Implementación del patrón Singleton, este método retorna la instancia 
     * única del objeto PrintWriter para realizar los registros
     * 
     * @return objeto de escritura
     */
    private static PrintWriter getLogWriter()
    {
        try
        {
            String filename = Log.getFilename();
            if( Log.writerInstance == null )
            {
                String file = Log.logsDirectoryPathName + filename;
                Log.writerInstance = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
                System.out.println( (new File(file)).getCanonicalPath() );
            }
            return Log.writerInstance;
        }
        catch( Exception e )
        {
            System.err.println("No se puede cargar el log.");
            e.printStackTrace(System.err);
        }
        return null;
    }
    
    /**
     * Construye y retorna el nombre del archivo en el cual se registrarán los Logs
     * 
     * @return nombre del archivo de logs
     */
    private static String getFilename()
    {
        Calendar calendar = Calendar.getInstance();
        String filename = calendar.get(Calendar.YEAR) + "-" +
                            Log.fix( calendar.get(Calendar.MONTH) + 1 ) + "-" + 
                            Log.fix( calendar.get(Calendar.DAY_OF_MONTH) ) + ".txt";
        return filename;
    }
    
    /**
     * Retorna la conversión a String del número pasado como parámetro, esta representación
     * convierte el número a una representación de 2 números, agregando un cero de se necesario
     * 
     * @param number numero a ser convertido a String
     * @return representación en String de número con 2 cifras
     */
    private static String fix( int number )
    {
        return number < 10 ? "0"+number :  "" + number;
    }
    
    /**
     * Almacena un registro en el archivo de Log
     * 
     * @param type tipo del registro de log
     * @param tag etiqueta del registro de log
     * @param message mensaje de log
     */
    private static void register( String type, String message )
    {
        Exception exception = new Exception();
        String className = exception.getStackTrace()[2].getClassName();
        String methodName = exception.getStackTrace()[2].getMethodName();
        int line = exception.getStackTrace()[2].getLineNumber();
        String tag = className + "." + methodName + ":" + line;
        
        PrintWriter writer = Log.getLogWriter();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = dateFormatter.format(new Date());
        String newLine = System.getProperty("line.separator");
        writer.append( String.format("%s - [%s] %s : %s"+newLine, date, type, tag, message) );
        writer.flush();
    }
    
    /**
     * Registra un mensaje de información en el Log
     * 
     * @param message mensaje a registrar
     */
    public static void info( String message )
    {
        Log.register("INFO", message);
    }
    
    /**
     * Registra un mensaje de advertencia en el Log
     * 
     * @param message mensaje a registrar
     */
    public static void warning( String message )
    {
        Log.register("WARNING", message);
    }
    
    /**
     * Registra un mensaje de error en el Log
     * 
     * @param message mensaje a registrar
     */
    public static void error( String message )
    {
        
        
        Log.register("ERROR", message);
    }
    
}
