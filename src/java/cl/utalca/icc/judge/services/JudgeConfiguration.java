/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.utalca.icc.judge.services;

import cl.utalca.icc.judge.IOUtils;
import cl.utalca.icc.judge.language.Language;
import cl.utalca.icc.judge.language.LanguagesLoader;
import cl.utalca.icc.judge.log.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

/**
 *
 * @author pabrojas
 */
class JudgeConfiguration
{
    static private final String WEB_INF = "WEB-INF";
    static private final String CONFIG_FILE = "directories.properties";
    
    static private File TEMP_FOLDER;
    static private File LANGUAGE_DESCRIPTORS_FOLDER;
    static private Map<String, Language> languages;
    
    
    static public synchronized void initialize(WebServiceContext webServiceContext)
    {
        if( JudgeConfiguration.successfulyLoaded() )
        {
            return;
        }

        MessageContext messageContext = webServiceContext.getMessageContext();
        ServletContext servletContext = (ServletContext) messageContext.get(MessageContext.SERVLET_CONTEXT);
        String webInfPath = servletContext.getRealPath(JudgeConfiguration.WEB_INF);

        File file = new File(webInfPath + File.separator + JudgeConfiguration.CONFIG_FILE);
        System.out.println(file.getAbsolutePath());
        try
        {
            InputStream input = new FileInputStream(file);
            Properties properties = new Properties();
            properties.load(input);

            String compilingAndExecutionFolderValue = (String)properties.getProperty(
                    JudgeConfiguration.ConfigurationProperties.COMPILING_EXECUTION_FOLDER.toString());
            String languageDescriptorsFolderValue = (String)properties.getProperty(
                    JudgeConfiguration.ConfigurationProperties.LANGUAGE_DESCRIPTORS_FOLDER.toString());

            if( compilingAndExecutionFolderValue == null || languageDescriptorsFolderValue == null )
            {
                String message = String.format("El archivo de configuración no respeta el formato (%s)", file.getAbsoluteFile());
                Log.error(message);
                return;
            }

            JudgeConfiguration.TEMP_FOLDER = new File(compilingAndExecutionFolderValue);
            if( !JudgeConfiguration.TEMP_FOLDER.isAbsolute() )
            {
                JudgeConfiguration.TEMP_FOLDER = new File(webInfPath + File.separator + compilingAndExecutionFolderValue);
            }

            JudgeConfiguration.LANGUAGE_DESCRIPTORS_FOLDER = new File(languageDescriptorsFolderValue);
            if( !JudgeConfiguration.LANGUAGE_DESCRIPTORS_FOLDER.isAbsolute() )
            {
                JudgeConfiguration.LANGUAGE_DESCRIPTORS_FOLDER = new File(webInfPath + File.separator + languageDescriptorsFolderValue);
            }

            boolean languageDescriptorFolderIsDirectory = JudgeConfiguration.LANGUAGE_DESCRIPTORS_FOLDER.isDirectory();
            boolean tempFolderExists = JudgeConfiguration.TEMP_FOLDER.exists();
            boolean tempFolderIsFile = JudgeConfiguration.TEMP_FOLDER.isFile();

            if(!languageDescriptorFolderIsDirectory)
            {
                JudgeConfiguration.LANGUAGE_DESCRIPTORS_FOLDER = null;
                JudgeConfiguration.TEMP_FOLDER = null;
                Log.error("El valor de 'language-descriptors-folder' no corresponde a un directorio");
                return;
            }

            if(tempFolderExists && tempFolderIsFile)
            {
                JudgeConfiguration.LANGUAGE_DESCRIPTORS_FOLDER = null;
                JudgeConfiguration.TEMP_FOLDER = null;
                Log.error("El valor de 'compiling-execution-folder' existe y corresponde a un archivo");
                return;
            }

            JudgeConfiguration.TEMP_FOLDER.mkdirs();
            JudgeConfiguration.TEMP_FOLDER.mkdir();

            JudgeConfiguration.languages = LanguagesLoader.load(JudgeConfiguration.LANGUAGE_DESCRIPTORS_FOLDER);

        }
        catch( Exception e )
        {
            System.out.println(e);
            e.printStackTrace();
            JudgeConfiguration.TEMP_FOLDER = null;
            JudgeConfiguration.LANGUAGE_DESCRIPTORS_FOLDER = null;
            String message = String.format("No se puede acceder al archivo '%s'. Compruebe que existe y que tiene los permisos de lectura necesarios", 
                            file.getAbsoluteFile());
            Log.error(message);

        }
    }
    
    static public synchronized boolean successfulyLoaded()
    {
        boolean state = false;
        state = JudgeConfiguration.LANGUAGE_DESCRIPTORS_FOLDER != null && 
                JudgeConfiguration.TEMP_FOLDER != null &&
                JudgeConfiguration.LANGUAGE_DESCRIPTORS_FOLDER.isDirectory() &&
                JudgeConfiguration.TEMP_FOLDER.isDirectory();
        return state;
    }
    
    static public synchronized Language getLanguage(String description)
    {
        Language language;
        language = JudgeConfiguration.languages.get(description);
        language = language.clone();
        return language;
    }
    
    static public synchronized File getTempDirectory()
    {
        File directory;
        System.out.println("TEMP : " + JudgeConfiguration.TEMP_FOLDER);
        directory = IOUtils.createTempFolder(JudgeConfiguration.TEMP_FOLDER);
        System.out.println("VAMOS : " + directory);
        return directory;
    }
    
    
    /**
     * Enumeración que contiene los nombres de las claves del archivo
     * "directories.properties"
     */
    private enum ConfigurationProperties
    {
        COMPILING_EXECUTION_FOLDER("compiling-execution-folder"), 
        LANGUAGE_DESCRIPTORS_FOLDER("language-descriptors-folder");
        
        private String key;
        private ConfigurationProperties( String key )
        {
            this.key = key;
        }
        
        public String toString()
        {
            return this.key;
        }
    }
    
}
