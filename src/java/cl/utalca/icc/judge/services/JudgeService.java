/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.utalca.icc.judge.services;

import cl.utalca.icc.judge.Downloader;
import cl.utalca.icc.judge.IOUtils;
import cl.utalca.icc.judge.IncorrectFileTypeException;
import cl.utalca.icc.judge.Robot;
import cl.utalca.icc.judge.TestCaseFormatException;
import cl.utalca.icc.judge.language.Language;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

import javax.xml.ws.WebServiceContext;

/**
 *
 * @author pabrojas
 */
@WebService(serviceName = "JudgeService")
public class JudgeService 
{
    @Resource
    private WebServiceContext webServiceContext;
    
    
    @WebMethod(operationName = "test")
    public String test(
            @WebParam(name = "sourceFileURL") String sourceFileURL,
            @WebParam(name = "dataTestFileURL") String dataTestFileURL,
            @WebParam(name = "languageDescription") String languageDescription,
            @WebParam(name = "maximumExecutionMillis") long maximumExecutionMillis) 
    {
        JudgeConfiguration.initialize(this.webServiceContext);
        if( JudgeConfiguration.successfulyLoaded() )
        {
            try
            {
                File tempDirectory = JudgeConfiguration.getTempDirectory();

                URL sourceURL = new URL(sourceFileURL);
                URL dataTestURL = new URL(dataTestFileURL);
                File source = Downloader.fetch(sourceURL, tempDirectory);
                File testCase = Downloader.fetch(dataTestURL, tempDirectory);
                Language language = JudgeConfiguration.getLanguage(languageDescription);
                
                Robot robot = new Robot(tempDirectory, source, testCase, language, maximumExecutionMillis);
                String response = robot.test();
                //IOUtils.delete(tempDirectory);
                
                return response;
            }
            catch(MalformedURLException e)
            {
                return "Error en las URLs";
            }
            catch(IOException e)
            {
                return "Problema con los permisos de los directorios";
            }
            catch(IncorrectFileTypeException e)
            {
                return "Error en la extensión del archivo";
            }
            catch(TestCaseFormatException e)
            {
                return "Error en archivo de prueba";
            }
        }
        else
        {
            return "se cayo el sistema ya que esta mal configurado";
        }
    }
    
    
}
